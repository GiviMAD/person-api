# Readme

## Overview
This project serve an http rest api with two resources (Person and Address). Data is stored in mongoDb (using Waterline as ORM). 

Repositories, routers and most of the public functions are tested (using chai + chai-http + spy). Run command `npm test` after install to run the test. Waterline in-memory adapter is used for the test to not require a mongoDb instance running and provides a clean db each execution.

The project code is located under the src folder divided in three folders (api,dal and quick-validator) that will be described bellow.

The project is deployed in an aws server using the `docker-compose.yml` file that could be found at the root folder. Url: `ec2-18-216-123-221.us-east-2.compute.amazonaws.com`.

A postman collection to test the deployed app can be found at the root folder.

## Project folders

The code of the project is in 3 different folders:

### - dal (Data Access Logic)
    This folder contains the project model's definitions and the code related with the data storage. Waterline related code is under its own folder. 
    I access to the data through the repositories trying to isolate waterline dependencies from the rest of the project.
    There are two resources in this project Person and Address with an 1 to n relationship.

### - server
    Contains the code related to rest api. 
    I take advantage of one interface to reduce and simplify the router configuration code. 
    And also from an abstract class to decrease de development time of each resource route and made the code more self explanatory.

### - quick-validator
    This is a project focus on generate a validation function for a class instance based on the metadata that we expose using decorators in the class definition properties.
    With this I want to provide a sample of a reusable functionality and at the same time show how decorators could be use to hide complex behaviors and made the project code simpler.
    I want too to provide a sample of an express request handler (middleware), this could be found under the utils folder.

## Dev.
Task to debug project code under visual studio code are defined. 

There are three supported `environments` in this project defined by the environment variable NODE_ENV:

* `"prod"`: used by the docker container change db host config to "mongo". Server use port 80.

* `"test"`: used to run the test. Prevent the default execution of the app and change waterline adapter to sales-memory. Server use port 3000.

* `"dev"`: default if not provided or any other value. Require a mongo db running in localhost. Server use port 3000.

