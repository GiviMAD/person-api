import * as webpack from 'webpack';
import * as path from 'path';
const merge = require('webpack-merge');
import commonConfig from './webpack.config.common';

const config= merge(commonConfig, {
    entry: './src/app',
    output: {
      path: path.join(__dirname, 'build'),
      filename: 'backend.js'
    },
    devtool: 'inline-source-map'
 } as webpack.Configuration);
export default config;