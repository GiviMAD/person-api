FROM node:8
RUN mkdir /app
WORKDIR /app
COPY package.json /app
COPY package-lock.json /app
RUN npm install
COPY . /app
RUN npm test
RUN npm run build

EXPOSE 80
CMD ["npm", "start"]