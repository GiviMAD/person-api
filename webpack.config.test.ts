import * as webpack from 'webpack';
import * as path from 'path';
const merge = require('webpack-merge');
import commonConfig from './webpack.config.common';

const config = merge(commonConfig, {
  entry: './test.ts',

  output: {
    devtoolModuleFilenameTemplate: '[absolute-resource-path]',
    devtoolFallbackModuleFilenameTemplate: '[absolute-resource-path]?[hash]',
    path: path.join(__dirname, 'build'),
    filename: 'backend.spec.js'
  },
  module: {
    rules: [
      {
        test: /\.(js|ts)/,
        include: path.resolve('src'),
        loader: 'istanbul-instrumenter-loader',
        enforce: 'post',
      },
    ]
  },
  devtool: "inline-cheap-module-source-map" as any
} as webpack.Configuration);
export default config;