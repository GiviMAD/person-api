import { ServerManager } from './server-manager';
import { WaterlineManager } from './../dal/waterline-utils/waterline-manager';
import * as sinon from 'sinon';

describe('RestApiServerTest', () => {
  let server: ServerManager;
  let backFn:Function;
  before(()=>{
      backFn=WaterlineManager.getCollections;
      WaterlineManager.getCollections=sinon.stub().returns({});
  })
  it('tears up works!', async () => {
    server = new ServerManager();
    await server.tearUp();
  });
  it('tears down works!', async () => {
    await server.tearDown();
  });
  after(()=>{
    WaterlineManager.getCollections=<any>backFn;
  });
});
