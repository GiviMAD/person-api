import { Express, Request, Response, NextFunction } from 'express';
import { IResourceRouter } from "./api/routers/i-resource-router";
import { PersonRouter } from "./api/routers/person-router";
import * as debug from 'debug';
import * as express from 'express';
import { RestApiServer } from "./api/rest-api";
import { Server } from 'http';
import { AddressRouter } from './api/routers/address-router';


export class ServerManager {
    server: Server;
    expressApp: Express;
    serverBind: string;
    port: string;
    debug: debug.IDebugger;

    private readonly resourceRouters: IResourceRouter[] = [
        new PersonRouter(),
        new AddressRouter(),
    ];
    constructor(port: string = "3000") {
        this.debug = debug('server');
        this.port = port;
        this.serverBind = `Port ${this.port}`;
    }
    tearUp(): Promise<Express> {
        this.expressApp = express();
        let restApi = new RestApiServer(this.resourceRouters);
        restApi.use(this.expressApp);
		this.expressApp.use(this.handleRouteNotFound);
		this.expressApp.use(this.handleRouteError);
        return new Promise<Express>((resolve, reject) => {
            this.server = this.expressApp.listen(this.port, () => {
                this.onListening();
                resolve(this.expressApp);
            });
            this.server.on('error', this.onError.bind(this));
        });
    }
    tearDown(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            try {
                this.server.close(() => {
                    resolve();
                });
            } catch (error) {
                /* istanbul ignore next */
                reject(error);
            }
        });
    }

    private handleRouteNotFound(req: Request, res: Response, next: NextFunction) {
		var err: any = {};
		err.status = 404;
		next(err);
	}

	private handleRouteError(err:any, req: Request, res: Response, next: NextFunction) {
		if (err.status == 404) {
            res.status(500);
            res.json({ message: "Resource not found." });
            return;
        }
        let error:Error=err;
        //Global error handler
		res.status(500);
        res.json({ message: "Internal server error." });
	}

    //From express default app
    /**
     * Event listener for HTTP server "listening" event.
     */
    private onListening() {
        var addr = this.server.address();
        var bind = 'port ' + addr.port;
        this.debug('Listening on ' + bind);
    }
    /* istanbul ignore next */
	/**
	 * Event listener for HTTP server "error" event.
	 */
    private onError(error: NodeJS.ErrnoException) {
        if (error.syscall !== 'listen') {
            throw error;
        }
        // handle specific listen errors with friendly messages
        switch (error.code) {
            case 'EACCES':
                console.error(this.serverBind + ' requires elevated privileges');
                process.exit(1);
                break;
            case 'EADDRINUSE':
                console.error(this.serverBind + ' is already in use');
                process.exit(1);
                break;
            default:
                throw error;
        }
    }
}