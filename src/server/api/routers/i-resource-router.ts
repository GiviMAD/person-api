import { Router } from "express";

export interface IResourceRouter{
    resourceName:string;
    getRouter():Router;
}