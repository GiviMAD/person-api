import { AbstractResourceRouter } from "./abstract-resource-route";
import { Router, Request, Response, NextFunction } from "express";
import { AddressRepository } from "../../../dal/repositories/address-repository";
import { Address } from "../../../dal/models/address";
import { QuickValidatorMiddleware } from "../../../quick-validator/utils/middleware";

export class AddressRouter extends AbstractResourceRouter<Address> {
    constructor(){
        super("address");
    }
    private repository = new AddressRepository();

    private async get(req: Request, res: Response, next: NextFunction) {
        try {
            this.ok(res, (await this.repository.get()));
        } catch (error) {
            /* istanbul ignore next */
            next(error);
        }
    }
    private async getOne(req: Request, res: Response, next: NextFunction) {
        try {
            const id = req.params["id"];
            let address = await this.repository.getOne(id);
            if (!address) {
                this.notFound(res);
                return;
            }
            this.ok(res, address);
        } catch (error) {
            /* istanbul ignore next */
            next(error);
        }

    }
    private async post(req: Request, res: Response, next: NextFunction) {
        try {
            let address = req.body;
            if (!address) {
                this.badRequest(res, "Address data is required.");
                return;
            }else if (address.id){
                this.badRequest(res, "Address id is not empty.");
                return;
            }
            let dbAddress = await this.repository.insert(address);
            this.ok(res, dbAddress, true);
        } catch (error) {
            /* istanbul ignore next */
            next(error);
        }
    }
    private async put(req: Request, res: Response, next: NextFunction) {
        try {
            let address = req.body;
            if (!address) {
                this.badRequest(res, "Address data is required.");
                return;
            }else if (!address.id){
                this.badRequest(res, "Address id is required to update its data.");
                return;
            }
            this.ok(res, (await this.repository.update(address)));
        } catch (error) {
            /* istanbul ignore next */
            next(error);
        }
    }

    private async delete(req: Request, res: Response, next: NextFunction) {
        try {
            const id = req.params["id"];
            await this.repository.delete(id);
            this.ok(res);
        } catch (error) {
            /* istanbul ignore next */
            next(error);
        }
    }


    getRouter(): Router {
        let router = Router();
        router.use(QuickValidatorMiddleware(Address));
        router.get("", this.get.bind(this));
        router.get("/:id", this.getOne.bind(this));
        router.post("", this.post.bind(this));
        router.put("", this.put.bind(this));
        router.delete("/:id", this.delete.bind(this));
        return router;
    }

}