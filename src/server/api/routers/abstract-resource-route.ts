import { HttpStatusCodes } from './../utils/http-status-codes';
import { IResourceRouter } from './i-resource-router';
import { Router, Request, Response, NextFunction } from "express";

export abstract class AbstractResourceRouter<T> implements IResourceRouter{
    constructor(public resourceName: string){}
    abstract getRouter(): Router;

    protected badRequest(res: Response, reason: string) {
        res.status(HttpStatusCodes.BAD_REQUEST);
        res.json(`Bad Request: ${reason}`);
        return;
    }
    protected notFound(res: Response) {
        res.status(HttpStatusCodes.NOT_FOUND);
        res.json(`Not found`);
        return;
    }
    protected ok(res: Response, resource?: T | T[], created?: boolean) {
        res.status(created ? HttpStatusCodes.CREATED : resource ? HttpStatusCodes.OK : HttpStatusCodes.NO_CONTENT);
        res.json(resource);
        return;
    }
}