import { AbstractResourceRouter } from "./abstract-resource-route";
import { Router, Request, Response, NextFunction } from "express";
import { PersonRepository } from "../../../dal/repositories/person-repository";
import { Person } from "../../../dal/models/person";
import { QuickValidatorMiddleware } from "../../../quick-validator/utils/middleware";

export class PersonRouter extends AbstractResourceRouter<Person> {
    constructor(){
        super("person");
    }
    private repository = new PersonRepository();

    private async get(req: Request, res: Response, next: NextFunction) {
        try {
            this.ok(res, (await this.repository.get(this.shouldPopulateAddress(req))));
        } catch (error) {
            /* istanbul ignore next */
            next(error);
        }
    }
    private async getOne(req: Request, res: Response, next: NextFunction) {
        try {
            const id = req.params["id"];
            let person = await this.repository.getOne(id,this.shouldPopulateAddress(req));
            if (!person) {
                this.notFound(res);
                return;
            }
            this.ok(res, person);
        } catch (error) {
            /* istanbul ignore next */
            next(error);
        }

    }
    private shouldPopulateAddress(req:Request){
        if(req.query["includeAddress"]){
            return true;
        }
        return false;
    }
    private async post(req: Request, res: Response, next: NextFunction) {
        try {
            let person = req.body;
            /* istanbul ignore next */
            if (person.id){
                this.badRequest(res, "Person id is not empty.");
                return;
            }
            let dbPerson = await this.repository.insert(person);
            this.ok(res, dbPerson, true);
        } catch (error) {
            /* istanbul ignore next */
            next(error);
        }
    }
    private async put(req: Request, res: Response, next: NextFunction) {
        try {
            let person = req.body;
            /* istanbul ignore next */
            if (!person.id){
                this.badRequest(res, "Person id is required to update its data.");
                return;
            }
            this.ok(res, (await this.repository.update(person)));
        } catch (error) {
            /* istanbul ignore next */
            next(error);
        }
    }

    private async delete(req: Request, res: Response, next: NextFunction) {
        try {
            const id = req.params["id"];
            await this.repository.delete(id);
            this.ok(res);
        } catch (error) {
            /* istanbul ignore next */
            next(error);
        }
    }


    getRouter(): Router {
        let router = Router();
        router.use(QuickValidatorMiddleware(Person));
        router.get("", this.get.bind(this));
        router.get("/:id", this.getOne.bind(this));
        router.post("", this.post.bind(this));
        router.put("", this.put.bind(this));
        router.delete("/:id", this.delete.bind(this));
        return router;
    }

}