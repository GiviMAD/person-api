import { Person } from './../../../dal/models/person';
import { WaterlineManager } from './../../../dal/waterline-utils/waterline-manager';
import { PersonRouter } from "./person-router";
import * as chai from 'chai';
import { expect, request } from 'chai';

import * as sinon from 'sinon';
import { Router, Express } from 'express';
import * as express from 'express';
import { RestApiServer } from "../rest-api";
import { Server } from "http";
import { AddressRepository } from '../../../dal/repositories/address-repository';
import { PersonRepository } from '../../../dal/repositories/person-repository';
let chaiHttp = require('chai-http');
chai.use(chaiHttp);

describe('PersonRouter', () => {
    let app: Express;
    let server: Server;
    let person: Person;
    before(async () => {
        await WaterlineManager.tearUp();
        app = express();
        let api = new RestApiServer([new PersonRouter()], "api");
        api.use(app);
        server = app.listen(2000);

    })
    it('should allow post person', async () => {
        let req = request(app)
            .post('/api/person')
            .send({ firstName: 'Miguel', lastName: 'Álvarez', idDocument: '1300034', } as Person);
        let res = await req;
        expect(res).to.have.status(201);
        person = res.body as Person;
    });
    
    it('should allow put person', async () => {
        person.lastName="Alvarez Diez";
        let req = request(app)
            .put('/api/person')
            .send(person);
        let res = await req;
        expect(res).to.have.status(200);
        person = res.body as Person;
    });
    it('should return bad request when try to put without id.', async () => {
        try {
            let req = request(app)
            .put('/api/person')
            .send({ firstName:"Test"} as Person);
        } catch (error) {
            expect(error).to.have.status(400);
        }
    });
    it('should allow get person', async () => {
        let res = await request(app)
            .get('/api/person');
        expect(res.body.addresses).to.be.equals(undefined);
        expect(res).to.have.status(200);
    });
    it('should allow get person by id', async () => {
        let res = await request(app)
            .get(`/api/person/${person.id}`);
        expect(res).to.have.status(200);
    });
    it('should return not found when id is not found', async () => {
        try {
            let res = await request(app)
                .get(`/api/person/XXXXX`);
        } catch (error) {
            expect(error).to.have.status(404);
        }
    });
    it('should allow delete person', async () => {
        let res = await request(app)
            .del(`/api/person/${person.id}`);
        expect(res).to.have.status(204);
    });
    it('should fail if some validation middleware rule is break on post', async () => {
        try {
            let res = await request(app)
                .post('/api/person')
                .send({ firstName: '', lastName: 'a', idDocument: '1300034', } as Person);
        } catch (error) {
            expect(error).to.have.status(400);
            expect(error.response.text).not.to.be.equal(undefined);
        }
    });
    it('should fail if post body contains id', async () => {
        try {
            let res = await request(app)
                .post('/api/person')
                .send({ id:"someID", firstName: '', lastName: 'a', idDocument: '1300034', } as Person);
        } catch (error) {
            expect(error).to.have.status(400);
        }
    });

    it('should allow include person related addresses in get', async () => {
        let addressRepository = new AddressRepository();
        let personRepository = new PersonRepository();
        let dbPerson = await personRepository.insert({
            firstName: "Miguel",
            lastName: "",
            age: 25,
            addresses: [],
            idDocument: "1111",
        });
        expect(dbPerson).not.to.be.equals(undefined);
        let dbAddress = await addressRepository.insert({
            street: "Any Street",
            city: "A Coruña",
            country: "Spain",
            person: dbPerson.id,
        });
        dbPerson = (await request(app).get(`/api/person/${dbPerson.id}?includeAddress=true`)).body as Person;
        expect(dbPerson.addresses && dbPerson.addresses.length).to.be.equals(1);
        dbPerson = (await request(app).get('/api/person?includeAddress=true'))
            .body
            .find((p: Person) => p.id === dbPerson.id) as Person;
        expect(dbPerson.addresses.length).to.be.equals(1);
    });
    after(async () => {
        await new Promise((resolve) => {
            server.close(() => {
                resolve();
            });
        })
        await WaterlineManager.tearDown();
    });
});
