import { WaterlineManager } from './../../../dal/waterline-utils/waterline-manager';
import * as chai from 'chai';
import { expect, request } from 'chai';

import * as sinon from 'sinon';
import { Router, Express } from 'express';
import * as express from 'express';
import { RestApiServer } from "../rest-api";
import { Server } from "http";
import { AddressRouter } from './address-router';
import { Address } from '../../../dal/models/address';
import { PersonRepository } from '../../../dal/repositories/person-repository';
import { Person } from '../../../dal/models/person';
let chaiHttp = require('chai-http');
chai.use(chaiHttp);

describe('AddressRouter', () => {
    let app: Express;
    let server: Server;
    let address: Address;
    let personRepository: PersonRepository;
    let testPerson: Person;
    before(async () => {
        await WaterlineManager.tearUp();
        personRepository = new PersonRepository();
        testPerson = await personRepository.insert({
            firstName: "Miguel",
            lastName: "Álvarez",
            age: 25,
            addresses: [],
            idDocument: "222222",
        });
        app = express();
        let api = new RestApiServer([new AddressRouter()], "api");
        api.use(app);
        server = app.listen(2000);

    })
    it('should allow post address', async () => {
        let req = request(app)
            .post('/api/address')
            .send({ street: "Any Street", city: "A Coruña", country: "Spain", person: testPerson.id } as Address);
        let res = await req;
        expect(res).to.have.status(201);
        address = res.body as Address;
    });
    it('should allow get address', async () => {
        let res = await request(app)
            .get('/api/address');
        expect(res).to.have.status(200);
    });
    it('should allow get address by id', async () => {
        let res = await request(app)
            .get(`/api/address/${address.id}`);
        expect(res).to.have.status(200);
    });
    it('should allow delete address', async () => {
        let res = await request(app)
            .del(`/api/address/${address.id}`);
        expect(res).to.have.status(204);
    });
    it('should fail with bad request when person id is not provided', async () => {
        try {
            let res = await request(app)
                .post('/api/address')
                .send({ street: "Any Street", city: "A Coruña", country: "Spain" } as Address);
        } catch (error) {
            expect(error).to.have.status(400);
            expect(error.response.text).not.to.be.equal(undefined);
        }
    });
    after(async () => {
        await new Promise((resolve) => {
            server.close(() => {
                resolve();
            });
        })
        await WaterlineManager.tearDown();
    });
});
