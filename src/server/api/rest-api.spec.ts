import { expect } from 'chai';
import { RestApiServer } from './rest-api';
import * as sinon from 'sinon';
import { Router,Express } from 'express';


describe('RestApiServerTest', () => {
  let server: RestApiServer;
  let backFn:Function;
  before(()=>{
  })
  it('should route all resources', async () => {
    let callSpy=sinon.spy(()=>Router());
    server = new RestApiServer([{resourceName:'test',getRouter:callSpy}]);
    expect(callSpy.calledOnce).to.be.true;    
  });
  it('should call use in express app', async () => {
    server = new RestApiServer([{resourceName:'test',getRouter:()=>Router()}]);
    let callSpy=sinon.spy();
    server.use({use:callSpy as any} as Express);
    expect(callSpy.calledOnce).to.be.true;    
  });
  after(()=>{
  });
});
