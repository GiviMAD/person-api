import * as bodyParser from 'body-parser';
import * as helmet from 'helmet';
import * as compression from 'compression';
import * as logger from 'morgan';
import { Express, Router } from "express";
import { IResourceRouter } from './routers/i-resource-router';

export class RestApiServer {
    private readonly serverBind: string;
    private readonly port: string;
    private readonly debug: debug.IDebugger;
    private readonly apiRouter:Router;
    
    constructor(private readonly resourceRouters: IResourceRouter[],private readonly path: string = "api") {
        this.apiRouter = Router();
        this.addMiddlewares(this.apiRouter);
        this.addResources(this.apiRouter, this.resourceRouters);
    }
    public use(expressApp:Express,){
        expressApp.use(`/${this.path}`, this.apiRouter);
    }
    private addMiddlewares(router: Router) {
        router.use(logger('dev'));//TODO:review logger
        router.use(helmet());
        router.use(compression());
        router.use(bodyParser.json());
    }
    private addResources(apiRouter: Router, resourceRouters: IResourceRouter[]) {
        for (let resourceRouter of resourceRouters) {
            apiRouter.use(`/${resourceRouter.resourceName}`, resourceRouter.getRouter());
        }
    }

}
