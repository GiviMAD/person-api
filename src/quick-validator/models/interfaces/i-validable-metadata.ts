import { IValidablePropertyMetadata } from "./i-validable-property-metadata";

export interface IValidableMetadata {
    propertiesMetadataMap: { [_id: string]: IValidablePropertyMetadata };
}
