import { IValidablePropertyMetadataOptions } from "./i-validable-property-metadata-options";

export interface IValidablePropertyMetadata {
    validations: { key: Symbol, value: any }[];
    options: IValidablePropertyMetadataOptions;
}