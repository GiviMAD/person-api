import { IValidableMetadata } from "./i-validable-metadata";

export interface IValidableObject {
    __ValidableMetadata?: IValidableMetadata;
}