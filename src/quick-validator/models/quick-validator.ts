import { AbstractPropertyValidator } from "./property-validators/abstract-property-validator";
import { RequiredPropertyValidator } from "./property-validators/required-property-validator";
import { MaxLengthPropertyValidator } from "./property-validators/max-length-property-validator";
import { MinLengthPropertyValidator } from "./property-validators/min-length-property-validator";
import { IValidableObject,IValidableMetadata } from "./interfaces";
export class QuickValidator<T>{
    private static validators: AbstractPropertyValidator[] = [new RequiredPropertyValidator(), new MaxLengthPropertyValidator(), new MinLengthPropertyValidator()];
    private constructor(private readonly validationFunction: (model: T) => ValidationResult) {

    }
    validate(model: T): ValidationResult {
        return this.validationFunction(model);
    }
    static fromClass<T>(modelClass: { new(...args: any[]): T, prototype?: Object & IValidableObject }): QuickValidator<T> {
        let metadata = modelClass.prototype && modelClass.prototype.__ValidableMetadata;
        if (!metadata)
            throw new Error("Error trying to generate a validator from a no validable class");
        let validationDataArray = this.getValidationsData(modelClass.name, metadata);
        return new QuickValidator(instance => {
            let errMessages: string[] = [];
            for (let validation of validationDataArray) {
                if (!validation.exec(instance)) {
                    errMessages.push(validation.supplyErrorMsg(instance));
                }
            }
            return new ValidationResult(errMessages);
        });
    }
    private static getValidationsData(modelName: string, metadata: IValidableMetadata, ) {
        let validationDataArray: { exec: (target: any) => boolean, supplyErrorMsg: (target: any) => string }[] = [];
        for (let key in metadata.propertiesMetadataMap) {
            const propertyConfig = metadata.propertiesMetadataMap[key];
            for (let config of propertyConfig.validations) {
                const validator = this.validators.find(v => config.key === v.metadataKey);
                if (!validator) {
                    throw new Error("Unknown validator");
                }
                validationDataArray.push({
                    exec: (target: any) => validator.validate(target[key], config.value),
                    supplyErrorMsg: (target) => validator.getErrorMsg(target[key], config.value, propertyConfig.options.displayName || key, modelName),
                });
            }
        }
        return validationDataArray;
    }
}
export class ValidationResult {
    constructor(public readonly errMessages: string[]) { }
    get invalid() {
        return !!this.errMessages.length;
    };
    get valid() {
        return !this.invalid;
    };
}