import { maxLengthMetadataKey } from '../../utils/symbols';
import { AbstractPropertyValidator } from './abstract-property-validator';

export class MaxLengthPropertyValidator extends AbstractPropertyValidator<string,number>{
    constructor() {
        super(maxLengthMetadataKey, [String]);
    }
    valueCheck(value: string, args: number): boolean {
        return !value || (!value.length && value.length !== 0) || value.length <= args;
    }
    getErrorMsg(value: string, args: number, propertyName: string, modelName: string): string {
        return `${propertyName} max length is ${args}. Actual length is ${value.length}.`;
    }
}
