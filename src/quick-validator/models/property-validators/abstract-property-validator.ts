import { requiredMetadataKey, maxLengthMetadataKey, minLengthMetadataKey } from '../../utils/symbols';
export abstract class AbstractPropertyValidator<ValueType=any,ArgsType=any>{
    constructor(public readonly metadataKey: string | Symbol, private readonly allowedTypes?: { new(...args: any[]): {} }[]) {
    }
    validate(value: ValueType, args: ArgsType):boolean {
        this.applyTypeCheck(value);
        return this.valueCheck(value, args);
    }
    abstract valueCheck(value: ValueType, args: ArgsType): boolean;
    abstract getErrorMsg(value: ValueType, args: ArgsType,propertyName:string,modelName:string): string;
    private applyTypeCheck(value: ValueType) {
        if (this.allowedTypes && (value || (typeof(value)=== "boolean" && value === false) || (typeof(value)=== "number" && value === 0)) && this.allowedTypes.some(type => value instanceof type)) {
            throw new Error("Value type exception");//TODO: Add more info
        }
    }
}
