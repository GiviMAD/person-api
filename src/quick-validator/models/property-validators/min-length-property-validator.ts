import { minLengthMetadataKey } from '../../utils/symbols';
import { AbstractPropertyValidator } from './abstract-property-validator';

export class MinLengthPropertyValidator extends AbstractPropertyValidator<string,number>{
    constructor() {
        super(minLengthMetadataKey, [String]);
    }
    valueCheck(value: string, args: number): boolean {
        return !value || (!value.length && value.length !== 0) || value.length >= args;
    }
    getErrorMsg(value: string, args: number, propertyName: string, modelName: string): string {
        return `${propertyName} min length is ${args}. Actual length is ${value.length}.`;
    }
}
