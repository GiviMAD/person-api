import { requiredMetadataKey } from '../../utils/symbols';
import { AbstractPropertyValidator } from './abstract-property-validator';

export class RequiredPropertyValidator extends AbstractPropertyValidator {
    constructor() {
        super(requiredMetadataKey);
    }
    valueCheck(value: any, args: any): boolean {
        return value === false || value === 0 || !!value;
    }
    getErrorMsg(value: any, args: any, propertyName: string, modelName: string): string {
        return `${propertyName} is required.`;
    }
}