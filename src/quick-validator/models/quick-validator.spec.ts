import { expect } from "chai";
import { QuickValidator } from './quick-validator';
import { required, minLength, maxLength, displayName } from "../utils/decorators";
import { addMetadataAction } from "../utils/metadata-accessors";

describe('QuickValidator', () => {
    class Test {
        @displayName("Identity")
        @required()
        id: number;
        @maxLength(6)
        @minLength(4)
        text: string;
    }
    class Test2{}
    it('should generate a validator from a decorated model', () => {
        let test = new Test();
        test.id = <any>undefined;
        let testValidator = QuickValidator.fromClass(Test);
        let validationResult = testValidator.validate(test);
        expect(validationResult.invalid).to.be.true;
        expect(validationResult.errMessages.length).to.be.equal(1);
        test.id = 1;
        validationResult = testValidator.validate(test);
        expect(validationResult.valid).to.be.true;
        test.text = "should fail";
        validationResult = testValidator.validate(test);
        expect(validationResult.invalid).to.be.true;
        expect(validationResult.errMessages.length).to.be.equal(1);
        test.text = "min";
        validationResult = testValidator.validate(test);
        expect(validationResult.invalid).to.be.true;
        expect(validationResult.errMessages.length).to.be.equal(1);
        test.text = "Works!";
        validationResult = testValidator.validate(test);
        expect(validationResult.valid).to.be.true;
    });
    it("should throw when try to generate a validator from an object without metadata",()=>{
        expect(()=>QuickValidator.fromClass(Test2)).throws();
    });
    it("should throw when find metadata from an unknown validator",()=>{
        function fakeDecorator(){
            return addMetadataAction(Symbol("UNKNOWN"),{});
        }
        class Test3{
            @fakeDecorator()
            id:string;
        }
        expect(()=>QuickValidator.fromClass(Test3)).throws();
    });
});