import { RequestHandler } from "express";
import { QuickValidator } from "../index";
export const QuickValidatorMiddleware = function <T>(classRef: { new(...args: any[]): T }): RequestHandler {
    const validator = QuickValidator.fromClass(classRef);
    return (req, res, next) => {
        if (["POST", "PUT"].includes(req.method)) {
            const model = req.body;
            let validationResult = validator.validate(model);
            if (validationResult.invalid) {
                let lineBreak = `\n\r`;
                let message = `Model validation fail: ${lineBreak}`;
                for (let errMsg of validationResult.errMessages) {
                    message += `- ${errMsg} ${lineBreak}`;
                }
                res.status(400);
                res.json({ message: message });
                return;
            }
        }
        next();
    }
}