import { IValidableObject, IValidableMetadata, IValidablePropertyMetadataOptions, IValidablePropertyMetadata } from "../models/interfaces";

function getValidableMetadata(target: IValidableObject):IValidableMetadata {
    return target.__ValidableMetadata ? target.__ValidableMetadata : target.__ValidableMetadata = {propertiesMetadataMap:{}};
}
function getPropertyMetadata(metadata: IValidableMetadata, propertyKey: string):IValidablePropertyMetadata {
    return metadata.propertiesMetadataMap[propertyKey] ?
        metadata.propertiesMetadataMap[propertyKey] :
        metadata.propertiesMetadataMap[propertyKey] = { validations: [], options: {} };
}
function getPropertyMetadataFromValidable(target:Object,propertyKey:string):IValidablePropertyMetadata{
    return getPropertyMetadata(getValidableMetadata(target), propertyKey);
}
/**
 * Add provided property validator metadata key to the container class metadata
 * @param validationMetadataKey 
 * Validator Symbol
 * @param args 
 * Arguments for validation
 */
export function addMetadataAction<ArgsType>(validationMetadataKey: Symbol, args: ArgsType) {
    return function (target: Object, propertyKey: string) {
        let propertyValidationConfig = getPropertyMetadataFromValidable(target,propertyKey);
        propertyValidationConfig.validations.push({ key: validationMetadataKey, value: args, });
    }
}
/**
 * Add provided property validator option key to the container class metadata
 * @param option 
 * @param value 
 */
export function addMetadataOption<OptionKey extends keyof IValidablePropertyMetadataOptions, ValueType extends IValidablePropertyMetadataOptions[OptionKey]>(option: OptionKey, value: ValueType) {
    return function (target: Object, propertyKey: string) {
        let propertyValidationConfig = getPropertyMetadataFromValidable(target,propertyKey);
        propertyValidationConfig.options[option] = value;
    }
}