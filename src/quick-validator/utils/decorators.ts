import { maxLengthMetadataKey, minLengthMetadataKey, requiredMetadataKey } from './symbols';
import { addMetadataAction, addMetadataOption } from './metadata-accessors';


//validations
/**
 * Add max length validation to this property
 * @param limit 
 * Max allowed length
 */
export function maxLength(limit: number) {
    return addMetadataAction(maxLengthMetadataKey, limit);
}

/**
 * Add min length validation to this property
 * @param limit 
 * Min allowed length
 */
export function minLength(limit: number) {
    return addMetadataAction(minLengthMetadataKey, limit);
}

/**
 * Add required validation to this property
 */
export function required() {
    return addMetadataAction(requiredMetadataKey, true);
}

//options
/**
 * Overwrite property name in error messages
 * @param name 
 * Name to display
 */
export function displayName(name: string) {
    return addMetadataOption("displayName", name);
}

