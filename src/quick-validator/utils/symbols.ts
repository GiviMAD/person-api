//validators
export const requiredMetadataKey = Symbol("required");
export const maxLengthMetadataKey = Symbol("maxLength");
export const minLengthMetadataKey = Symbol("minLength");

//options
export const displayNameMetadataKey = Symbol("displayName");