import { WaterlineManager } from "./dal/waterline-utils/waterline-manager";
import { ServerManager } from "./server/server-manager";

class App {
    /* istanbul ignore next */
    static async main() {
        //wait for orm initialization
        await WaterlineManager.tearUp();
        //wait for server initialization
        let server = new ServerManager(process.env.NODE_ENV==="prod"?"80":"3000");
        await server.tearUp();
        //server is running
    }
}
/* istanbul ignore next */
if (process.env.NODE_ENV !== "test") {
    App.main().catch(err => { throw err });
}