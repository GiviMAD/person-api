import { Person } from './../models/person';
import { PersonRepository } from './person-repository';
import { expect } from "chai";
import { WaterlineManager } from '../waterline-utils/waterline-manager';
import * as Waterline from "waterline";
import { AddressRepository } from './address-repository';


describe('PersonRepository', () => {
    let repository: PersonRepository;
    let addressRepository: AddressRepository;
    let testPerson:Person;
    before(async () => {
        await WaterlineManager.tearUp();
        repository = new PersonRepository();
    });
    it('should insert', async () => {
        let person=await repository.insert({
            firstName: "Miguel",
            lastName: "",
            age:25,
            addresses:[],
            idDocument:"1111",
        });
        expect(person).not.to.be.equals(undefined);
        expect(person.id).not.to.be.equals(undefined);
        testPerson=person;
    });
    it('should update', async () => {
        const lastName= "Álvarez Díez";
        testPerson.lastName=lastName;
        let person=await repository.update(testPerson);
        expect(person.lastName).to.be.equals(lastName);
        expect(person.id).not.to.be.equals(undefined);
        testPerson=person;
    });    
    it('should get all', async () => {
        let persons=await repository.get();
        expect(persons.length).to.be.at.least(1);
    });
    it('should get by id', async () => {
        let dbPerson=await repository.getOne(testPerson.id as string);
        expect(dbPerson).not.to.be.equals(undefined);
    });
    it('should delete by id', async () => {
        let dbPerson=await repository.getOne(testPerson.id as string);
        expect(dbPerson).not.to.be.equals(undefined);
        await repository.delete(testPerson.id as string);
        dbPerson=await repository.getOne(testPerson.id as string);
        expect(dbPerson).to.be.equals(undefined);
    });
    it('should allow include person related addresses', async () => {
        addressRepository=new AddressRepository();
        let dbPerson=await repository.insert({
            firstName: "Miguel",
            lastName: "",
            age:25,
            addresses:[],
            idDocument:"1111",
        });
        expect(dbPerson).not.to.be.equals(undefined);

        let dbAddress = await addressRepository.insert({
            street: "Any Street",
            city: "A Coruña",
            country: "Spain",
            person:dbPerson.id,
        });
        dbPerson=await repository.getOne(dbPerson.id as string,true);
        expect(dbPerson.addresses.length).to.be.equals(1);
        dbPerson=(await repository.get(true)).find((p:Person)=>p.id===dbPerson.id) as Person;
        expect(dbPerson.addresses.length).to.be.equals(1);
    });
    
    after(async () => {
        await WaterlineManager.tearDown();
    });
});