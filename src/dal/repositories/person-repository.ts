import { Person } from './../models/person';
import { WaterlineManager } from '../waterline-utils/waterline-manager';
import { Model,QueryBuilder, WaterlinePromise } from 'waterline';
import { WaterlineUtils } from '../waterline-utils/waterline-utils';

export class PersonRepository{
    personCollection: Model;
    constructor(){
        let collections=WaterlineManager.getCollections();
        this.personCollection=collections["person"];
    }
    get(populateAddresses?:boolean){
        let query=this.personCollection.find();
        if(populateAddresses){
            query=this.populateAddresses(query);
        }
        return WaterlineUtils.toNativePromise<Person[]>(query);
    }
    getOne(id:string,populateAddresses?:boolean){
        let query=this.personCollection.findOne({id:id});
        if(populateAddresses){
            query=this.populateAddresses(query);
        }
        return WaterlineUtils.toNativePromise<Person>(query);
    }
    insert(person:Person){
        return WaterlineUtils.toNativePromise<Person>(this.personCollection.create(person));
    }
    async update(person:Person){
        //we know only one row could be affected
        return (await WaterlineUtils.toNativePromise<Person[]>(this.personCollection.update({id:person.id},person)))[0];
    }
    delete(id:string){
        return WaterlineUtils.toNativePromise<Person[]>(this.personCollection.destroy({id:id}));
    }
    private populateAddresses<T>(query:QueryBuilder<T>):QueryBuilder<T>{
        return query.populate('addresses');
    }

}