import { Person } from './../models/person';
import { WaterlineManager } from '../waterline-utils/waterline-manager';
import { Model,QueryBuilder, WaterlinePromise } from 'waterline';
import { Address } from '../models/address';
import { WaterlineUtils } from '../waterline-utils/waterline-utils';

export class AddressRepository{
    addressCollection: Model;
    constructor(){
        let collections=WaterlineManager.getCollections();
        this.addressCollection=collections["address"];
    }
    get(){
        return WaterlineUtils.toNativePromise<Address[]>(this.addressCollection.find());
    }
    getOne(id:string){
        return WaterlineUtils.toNativePromise<Address>(this.addressCollection.findOne({id:id}));
    }
    insert(address:Address){
        return WaterlineUtils.toNativePromise<Address>(this.addressCollection.create(address));
    }
    async update(address:Address){
        return (await WaterlineUtils.toNativePromise<Address[]>(this.addressCollection.update({id:address.id},address)))[0];
    }
    delete(id:string){
        return WaterlineUtils.toNativePromise<Address[]>(this.addressCollection.destroy({id:id}));
    }
}