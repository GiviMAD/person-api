import { expect } from "chai";
import { WaterlineManager } from '../waterline-utils/waterline-manager';
import * as Waterline from "waterline";
import { Address } from "../models/address";
import { AddressRepository } from "./address-repository";
import { PersonRepository } from "./person-repository";
import { Person } from "../models/person";


describe('AddressRepository', () => {
    let repository: AddressRepository;
    let personRepository: PersonRepository;
    let testAddress: Address;
    let testPerson: Person;
    before(async () => {
        await WaterlineManager.tearUp();
        repository = new AddressRepository();
        personRepository = new PersonRepository();
        testPerson=await personRepository.insert({
            firstName: "Miguel",
            lastName: "Álvarez",
            age:25,
            addresses:[],
            idDocument:"222222",
        });
    });
    it('should insert', async () => {
        let address = await repository.insert({
            street: "Any Street",
            city: "A Coruña",
            country: "Spain",
            person:testPerson.id
        });
        expect(address).not.to.be.equals(undefined);
        expect(address.id).not.to.be.equals(undefined);
        testAddress = address;
    });
    it('should update', async () => {
        const testCity = "Lugo";
        testAddress.city = testCity;
        let address = await repository.update(testAddress);
        expect(address.city).to.be.equals(testCity);
        expect(address.id).not.to.be.equals(undefined);
        testAddress = address;
    });
    it('should get all', async () => {
        let addresses = await repository.get();
        expect(addresses.length).to.be.at.least(1);
    });
    it('should get by id', async () => {
        let dbAddress = await repository.getOne(testAddress.id as string);
        expect(dbAddress).not.to.be.equals(undefined);
    });
    it('should delete by id', async () => {
        let dbAddress = await repository.getOne(testAddress.id as string);
        expect(dbAddress).not.to.be.equals(undefined);
        await repository.delete(testAddress.id as string);
        dbAddress = await repository.getOne(testAddress.id as string);
        expect(dbAddress).to.be.equals(undefined);
    });

    after(async () => {
        await WaterlineManager.tearDown();
    });
});