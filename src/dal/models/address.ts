import { Person } from "./person";
import { required, displayName } from "../../quick-validator";

export class Address{
    id?:string;
    street?: string;
    city?: string;
    @displayName("Country")
    @required()
    country: string;
    /* istanbul ignore next */
    @displayName("Person id")
    @required()
    person?: string|Person;
}