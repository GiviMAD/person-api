import {required, minLength, maxLength, displayName} from "../../quick-validator";
import { Address } from "./address";

export class Person{
    id?:string;
    /* istanbul ignore next */
    @displayName("Fist Name")
    @required()
    @minLength(2)
    firstName: string;
    /* istanbul ignore next */
    @displayName("Last Name")
    @required()
    @minLength(2)
    lastName: string;
    age: number;
    /* istanbul ignore next */
    @displayName("Id Document")
    @required()
    @minLength(5)
    @maxLength(50)
    idDocument:string;
    addresses: Address[];
}