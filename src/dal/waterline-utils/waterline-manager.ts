import { config } from './config/waterline-config';
import { PersonCollection } from './collections/person-collection';
import * as Waterline from "waterline";
import { AddressCollection } from './collections/address-collection';
import { disconnect } from 'cluster';

type collectionNames="person"|"address";
export class WaterlineManager{
    private static models:{[name:string]:Waterline.Model}|null;
    private static waterline:Waterline.Waterline;
    private static readonly collections:Waterline.CollectionClass[]=[
        PersonCollection,
        AddressCollection
    ];
    static getCollections(){
        if(!this.models){
            throw new Error("Waterline is not initialized");
        }
        return this.models;
    }
    static tearUp(){
        return new Promise((resolve,reject)=>{
            const waterline = new Waterline();
            WaterlineManager.collections.forEach(collection => {                
                waterline.loadCollection(collection);
            });
            waterline.initialize(config, (err, ontology) => {
                if (err) {
                   return reject(err);
                }
                WaterlineManager.models= {
                    "person":ontology.collections.person,
                    "address":ontology.collections.address
                };
                WaterlineManager.waterline=waterline;
                resolve();
            });            
        });
    }
    static tearDown(){
        return new Promise((resolve,reject)=>{
            let disposable=WaterlineManager.waterline as {teardown?(cb:Function):void};
            if(disposable.teardown){
                disposable.teardown(()=>{
                    this.models=null;
                    resolve();
                });
            }else{
                reject(new Error("Unable to dispose db connection."));
            }
        });
    }
}