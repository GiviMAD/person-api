import * as Waterline from "waterline";
const mongoAdapter = require("sails-mongo");
const memoryAdapter = require("sails-memory");

let mongoDbAdapter = "sails-mongo";
let mongoDbHost: string | undefined;

switch (process.env.NODE_ENV) {
    case "test":
        //use in memory adapter in test env to not require mongo db running
        mongoDbAdapter = "sails-memory";
        break;
    /* istanbul ignore next */
    case "prod":
        mongoDbHost = "mongo";
        break;
    /* istanbul ignore next */
    default:
        mongoDbHost = undefined;
}
export const config: Waterline.Config & { defaults?: any } = {
    adapters: {
        'sails-mongo': mongoAdapter,
        'sails-memory': memoryAdapter,
    },
    connections: {
        mongoDb: {
            adapter: mongoDbAdapter,
            host: mongoDbHost,
        } as any,
    },
    defaults: {
        migrate: 'safe',
    }
};
