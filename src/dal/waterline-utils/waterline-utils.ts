import { WaterlinePromise } from "waterline";

export class WaterlineUtils{
    static toNativePromise<T>(waterlinePromise:WaterlinePromise<T>&{toPromise?():Promise<T>}):Promise<T>{
        if(!waterlinePromise.toPromise)
            throw new Error("Unable to convert to promise");
        return waterlinePromise.toPromise();
    }
}