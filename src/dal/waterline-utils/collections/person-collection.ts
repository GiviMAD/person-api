import * as Waterline from "waterline";
export let PersonCollection = Waterline.Collection.extend({
    identity: 'person',
    connection: 'mongoDb',
    migrate: 'safe',
    attributes: {
        firstName: 'string',
        lastName: 'string',
        age: 'integer',
        idDocument:{
            type:"string",
            required:true,
            maxLength:50
        },
        addresses: {
            collection: 'address',
            via: 'person',
            dominant: true
          }
    }
});