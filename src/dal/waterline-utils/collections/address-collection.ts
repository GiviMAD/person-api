import * as Waterline from "waterline";

export const AddressCollection=Waterline.Collection.extend(
    {
        identity: "address",
        connection: "mongoDb",
        migrate: 'safe',
        attributes: {
            street: "string",
            city: "string",
            country: "string",
            person: {
                model: "person",
            },
        },
    }
);