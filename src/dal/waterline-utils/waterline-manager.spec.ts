import { expect} from "chai";
import * as Waterline from "waterline";
import { WaterlineManager } from './waterline-manager';


describe('WaterlineManager', () => {
    it('should tears up',async ()=>{
        await WaterlineManager.tearUp();
    });
    it('should return collections dictionary', async () => {
        const collections=WaterlineManager.getCollections();
        expect(collections).not.to.be.equals(undefined);
        expect(Object.keys(collections).length).to.be.at.least(2);
    });
    it('should tears down',async ()=>{
        await WaterlineManager.tearDown();
    });

  });