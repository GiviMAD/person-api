import * as webpack from 'webpack';
import * as fs from 'fs';

var nodeModules = {};
fs.readdirSync('node_modules')
  .filter(function (x: string) {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach(function (mod: string) {
    (nodeModules as any)[mod] = 'commonjs ' + mod;
  });

const config = {
  target: 'node',
  externals: nodeModules,
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: ['.ts', '.tsx']
  },
  module: {
    rules: [
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      { test: /\.tsx?$/, loader: 'ts-loader' }
    ]
  },
} as webpack.Configuration;
export default config;